# Happy Numbers

How to discover if a number is a *happy* number:

1. It is a positive integer
2. Replace the number by the sum of squares of its digits
3. If the result is 1, the number is a *happy* number
4. If the result is not 1, repeat the process

Examples:

Is the number 7 a happy number ?

    7² = 49
    4² + 9² = 16 + 81 = 97
    9² + 7² = 81 + 49 = 130
    1² + 3² + 0² = 1 + 9 + 0 = 10
    1² + 0² = 1
    -> YAY! 7 is a *happy* number

Now let's take the number 4:

    4² = 16
    1² + 6² = 1 + 36 = 37
    3² + 7² = 9 + 49 = 58
    5² + 8² = 25 + 64 = 89
    8² + 9² = 64 + 81 = 145
    1² + 4² + 5² = 1 + 16 + 25 = 42
    4² + 2² = 16 + 4 = 20
    2² + 0² = 4 + 0 = 4
    -> and it starts all over again :( 4 is a *sad* number
