
def happy(n):
    n = sum(int(digit) ** 2 for digit in str(n))
    return n in (1, 7) if n < 10 else happy(n)


# -- tests --
assert all(happy(n) for n in (1, 7, 10, 97, 100, 130))
assert not all(happy(n) for n in (2, 3, 4, 5, 6, 8, 9, 20))
